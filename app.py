import math, random
from math import floor
from pathlib import Path

""" Fun with pathlib """

dir = Path("emails")
print(f'does {dir} exists {dir.exists()}')
print(f'Creating dir {dir}')
dir.mkdir()
print(f'does {dir} exists {dir.exists()}')
dir.rmdir()
exit(0)

""" Sample Dice impl """


class Dice:
    def roll(self):
        return (random.randint(1, 6), random.randint(1, 6))


dice = Dice()

for i in range(3):
    print(f'{dice.roll()}')


exit(0)

""" Use of random """
members = ['Teri' , 'Maan' ,  'Ki', 'Aankh']
for i in range(5):
    print(random.choice(members))
exit(0)

""" Module function call """
x = floor(3.5)
y = math.floor(3.5)

print(x, y)
exit(0)
""" Class Inheritance """


class Employee:
    name = ''
    empId = 0

    def __init__(self, name, empId):
        self.name = name
        self.empId = empId

    def  print_details(self):
        print(f'{self.name}, {self.empId}')

class Supervisor(Employee):
    pass


sup = Supervisor("John Doe", 12345)
sup.print_details()

exit(0)

""" Class Example """


class Person:
    name = ''

    def __init__(self, name):
        self.name = name

    def talk(self):
        print(f" Hi I am {self.name}, what is your name ?")

p =  Person("John Doe") #Person()

p.talk()
exit(0)

class Point:
    x = 0
    y = 0

    def __init__(self, x, y):
        self.x = x
        self.y = y

    def set_point(self, x, y):
        self.x = x
        self.y = y

    def draw(self):
        print(f"co-ordinates : {self.x}, {self.y}")

p = Point(10, 20)
p.draw()
p.set_point(2, 3)
p.draw()
exit(0)

""" Error handling """

try:
    age = int(input("Age ?"))
except ValueError:
    print(f" input has to be numeric")
exit(0)

"""Function example"""


def square(inp):
    return inp * inp


print(square(inp=2))
exit(0)

"""Emoji converter - use dict"""
emojis = {
    ":)" : "😀",
    ":(" : "🙁"
}
msg = input("> ").split(" ")
output = ""
for word in msg:
    output += emojis.get(word, word) + " "
print(output)
exit(0)

""" Tuples - can't modify unlike lists """
co_ord = (1, 2, 3)
nums = [4, 5, 6]
x, y, z = co_ord
print(x * y * z)
x, y, z = nums
print(x * y * z)
co_ord[0] = 5
exit(0)

""" Find dup numbers """
dup = [-1, 0, 2, -1 , 50, 100 , 2, 0]
for num in dup:
    while dup.count(num) > 1:
        dup.remove(num)
print(dup)
exit(0)

""" Fina max number in list """
num = [5, 2, 0, 1, 100]
max = 0;
for x in num:
    if x > max:
        max = x
print(max)
exit(0)

num = [5, 2, 5, 5, 2]
for x in num:
    print('x' * x)
exit(0)

prices =[10, 20, 30]
sum = 0
for price in prices:
    sum += price
print(f'Total price = {sum}')
exit(0)

""" Use of while & if """
while True:
    cmd = input("> ").lower()
    if cmd == 'start':
        print (f' started the car')
    elif cmd == 'stop':
        print(f' stopped the car')
    elif cmd == 'quit':
        print(f' exiting the game')
        break;
    elif cmd == 'help':
        print(f' valid commands : start, stop, help')
    else:
        print('in valid command -- type help')

exit(1)

""" Use of formatted string & input & casting to int"""
guess_limit = 3
guess_count = 0
secret_num = 9
guessed_num = 0;
while guess_count < guess_limit:
    guessed_num = int(input(f'Guess secret number({guess_limit - guess_count} attempts left) : '))
    if guessed_num == secret_num:
        print("You won")
        break
    guess_count += 1
else:
    print("You failed")
exit(1)

num = int(input("Enter a random num between 0 & 10 :"))
if num > 10 or num < 0:
    print(f'Invalid choice {num}')
elif num % 2 == 0:
    print(f' {num} is even')
else:
    print(f' {num} is odd')

is_hot = False
is_cold = True

if is_hot and is_cold:
    print("Wow it hot & cold")

if is_hot or is_cold:
    print("Wow its either hot or cold")

if is_hot:
    print('It is hot today')
elif is_cold:
    print('It is cold day')
else:
    print('neither cold/hot')

x = -2.9
print(math.floor(x))
print(math.ceil(x))
print(round(x))
print(abs(x))

print (10 + (3 * 2) ** 2)
print (10 + 3 * 2 ** 2)
print(10/3)
print(10//3)
print(10**3)

first = 'Varun'
last  = 'Sudan'
msg = f'{first} [{last}] is a coder'
print(msg.upper())
print(msg)
print(len(msg))
print(msg.find('S'))
print(msg.replace('Varun', 'Test'))
print(first in msg)


content = '''Hello , my friend
Run .. Run ..Run'''
print(content[0])
print(content[-1])
print(content[0:3])
print(content[1:-1])

print("What's up")
print('What"s up')

birth_year= input('Birth year : ')
age = 2020 - int(birth_year)
print(type(age))
print('age = ' , (2019 - int(birth_year)))


print("Varun Sudan")
print('*' * 10)

int = 20
print( int )

is_published = False
is_Published = True
print(is_published, is_Published)

name = input("What is ur name ?")
print('Hi ', name)

